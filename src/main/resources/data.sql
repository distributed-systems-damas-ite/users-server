DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(250) NOT NULL,
  username VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL
);

INSERT INTO users (id, name, username, password) VALUES
  (1, 'Admin', 'admin' , HASH('SHA256', STRINGTOUTF8('123456'), 1000));