package com.services.usersservice.services;

import com.services.usersservice.dto.LoginRequest;
import com.services.usersservice.dto.RegisterRequest;
import com.services.usersservice.models.User;
import com.services.usersservice.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsersService {

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private UsersRepository usersRepository;

    @Autowired
    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    private User findByUsername(String username) {
        return this.usersRepository.findFirstByUsername(username);
    }

    public User register(RegisterRequest registerRequest) {
        if (this.usersRepository.existsByUsername(registerRequest.getUsername())) { return null; }
        User user = new User();
        user.setName(registerRequest.getName());
        user.setUsername(registerRequest.getUsername());
        user.setPassword(passwordEncoder().encode(registerRequest.getPassword()));
        return this.usersRepository.save(user);
    }

    public User login(LoginRequest loginRequest) {
        User user = this.usersRepository.findFirstByUsername(loginRequest.getUsername());
        if (passwordEncoder().matches(loginRequest.getPassword(), user.getPassword())) {
            return user;
        } else {
            return null;
        }
    }
}
