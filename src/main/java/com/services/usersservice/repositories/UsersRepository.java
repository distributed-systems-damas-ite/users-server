package com.services.usersservice.repositories;

import com.services.usersservice.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    User findFirstByUsername(String username);
    Boolean existsByUsername(String username);
}
