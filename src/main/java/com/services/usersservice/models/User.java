package com.services.usersservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class User {

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    @NotNull(message = "name is required")
    @NotBlank(message = "name is required")
    private String name;

    @Column()
    @NotNull(message = "username is required")
    @NotBlank(message = "username is required")
    private String username;

    @Column()
    @NotNull(message = "password is required")
    @NotBlank(message = "password is required")
    @Length(min = 6, message = "password length must be greater than 6")
    @JsonIgnore()
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
