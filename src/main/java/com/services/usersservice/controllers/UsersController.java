package com.services.usersservice.controllers;

import com.services.usersservice.dto.LoginRequest;
import com.services.usersservice.dto.RegisterRequest;
import com.services.usersservice.models.User;
import com.services.usersservice.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping("users")
public class UsersController {

    private UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PostMapping("login")
    public User login(@Valid @RequestBody LoginRequest loginRequest) {
        return this.usersService.login(loginRequest);
    }

    @PostMapping("register")
    public User register(@Valid @RequestBody RegisterRequest registerRequest) {
        return this.usersService.register(registerRequest);
    }
}
